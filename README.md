# How to use K3S (as a developer)

> WIP 🚧

- create the cluster
- see `./commands`
- run k9s
- go to `./apps` and see `hello-js` (and create a new branch)
- go to `./tasks` and setup a Redis database
- go to `./apps` and see `hey-js` (and create a new branch)
- if you want to add monitoring features, go to `./monitoring`