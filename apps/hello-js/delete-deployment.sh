#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml

NAMESPACE="default"
BRANCH=$(git symbolic-ref --short HEAD)
APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))-${BRANCH}

kubectl delete deployment $APPLICATION_NAME -n $NAMESPACE
kubectl delete ingress $APPLICATION_NAME  -n $NAMESPACE


