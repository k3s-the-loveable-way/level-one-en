#!/bin/bash
export KUBECONFIG=$PWD/../config/k3s.yaml
kubectl -n kube-system get pods -l k8s-app=metrics-server

# metrics server is activated by default on K3S
