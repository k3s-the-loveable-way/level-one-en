#!/bin/sh

# Read environment variables
set -o allexport
source ./.env

multipass start ${vm_name}-1
multipass start ${vm_name}-2
multipass start ${vm_name}-3
multipass info ${vm_name}-1
multipass info ${vm_name}-2
multipass info ${vm_name}-3

