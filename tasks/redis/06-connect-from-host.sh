#!/bin/bash
export KUBECONFIG=$PWD/../../config/k3s.yaml
NAMESPACE="database"

kubectl port-forward --namespace ${NAMESPACE} svc/redis-mother 6379:6379 &

# connect to Medis with default settings
